package com.spring.sample.cidemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CiDemoApplication {

          /*
          first support
          */

	public static void main(String[] args) {
		SpringApplication.run(CiDemoApplication.class, args);
	}

}
