package com.spring.sample.cidemo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SimpleRestController {

	@RequestMapping(value="/test",method=RequestMethod.GET)
	public String hellotest() {
		return "hello-ci";
		
	}
}
